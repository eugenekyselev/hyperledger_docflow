'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const uuidv4 = require('uuid/v4');

const app = express();

let multer = require('multer');
let upload = multer();

var network = require('./scripts/network.js');

app.use(bodyParser.json());

app.post('/api/registerParticipant', function(req, res) {

  var accountNumber = uuidv4();
  var cardId = req.body.cardId;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;

  network.registerParticipant(cardId, accountNumber, firstName, lastName)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          success: response
        });
      }
    });
});

app.post('/api/createReport', upload.single('file'), function(req, res) {

  var cardId = req.body.cardId;
  var reportId = uuidv4();
  var description = req.body.description;
  var file = req.file;

  network.reportCreationTransaction(cardId, reportId, description, file)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          success: response
        });
      }
    });
});

app.post('/api/updateReport', upload.single('file'), function(req, res) {

  var cardId = req.body.cardId;
  var reportId = req.body.reportId;
  var file = req.file;

  network.reportUpdatingTransaction(cardId, reportId, file)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          success: response
        });
      }
    });
});

app.post('/api/participantData', function(req, res) {

  var cardId = req.body.cardId;

  var returnData = {};

  network.participantData(cardId)
    .then((employee) => {
      if (employee.error != null) {
        res.json({
          error: employee.error
        });
      } else {
        returnData.accountNumber = employee.id;
        returnData.firstName = employee.firstName;
        returnData.lastName = employee.lastName;
        returnData.amountOfReports = employee.amountOfReports;
      }
    })
    .then(() => {
      network.getParticipantReports(cardId)
        .then((reports) => {
          if (reports.error != null) {
            res.json({
              error: reports.error
            });
          } else {
            returnData.reports = reports;
          }

          res.json(returnData);
        });
    });
});

app.post('/api/employees', function(req, res) {

  var cardId = req.body.cardId;

  network.getAllEmployees(cardId)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          employees: response
        });
      }
    })
});

app.post('/api/versions', function(req, res) {

  var cardId = req.body.cardId;
  var reportId = req.body.reportId;

  network.getReportUpdatingTransactionInfo(cardId, reportId)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          reports: response
        });
      }
    })
});

app.post('/api/initially', function(req, res) {

  var cardId = req.body.cardId;
  var reportId = req.body.reportId;

  network.getReportCreationTransactionInfo(cardId, reportId)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          reports: response
        });
      }
    })
});

app.post('/api/employeeReports', function(req, res) {

  var accountNumber = req.body.accountNumber;
  var cardId = req.body.cardId;

  network.getEmployeeReports(cardId, accountNumber)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.json({
          reports: response
        });
      }
    });
});

var port = process.env.PORT || 8000;

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'client/build')));
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, function() {
  console.log('app running on port: %d', port);
});
