const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const { IdCard } = require('composer-common');

const ipfsAPI = require('ipfs-api');
const ipfs = ipfsAPI('localhost', '5001', {protocol: 'http'})

const namespace = 'org.nykredit.co';

let adminConnection;

let businessNetworkConnection;

let businessNetworkName = 'nykredit-network';
let factory;

async function importCardForIdentity(cardName, identity) {

  adminConnection = new AdminConnection();
  businessNetworkName = 'nykredit-network';

  const metadata = {
      userName: identity.userID,
      version: 1,
      enrollmentSecret: identity.userSecret,
      businessNetwork: businessNetworkName
  };

  const connectionProfile = require('./local_connection.json');
  const card = new IdCard(metadata, connectionProfile);

  await adminConnection.importCard(cardName, card);
}

module.exports = {

 registerParticipant: async function (cardId, accountNumber,firstName, lastName) {
    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect('admin@nykredit-network');

      factory = businessNetworkConnection.getBusinessNetwork().getFactory();

      const employee = factory.newResource(namespace, 'Employee', accountNumber);
      employee.firstName = firstName;
      employee.lastName = lastName;

      const participantRegistry = await businessNetworkConnection.getParticipantRegistry(namespace + '.Employee');
      await participantRegistry.add(employee);

      const identity = await businessNetworkConnection.issueIdentity(namespace + '.Employee#' + accountNumber, cardId);

      await importCardForIdentity(cardId, identity);

      await businessNetworkConnection.disconnect('admin@nykredit-network');

      return true;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },

  reportCreationTransaction: async function (cardId, reportId, description, file) {

    try {

      const ipfsHash = await ipfs.add(file.buffer);

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const pingConnection = await businessNetworkConnection.ping();
      const accountNumber = pingConnection.participant.split('#')[1];

      factory = businessNetworkConnection.getBusinessNetwork().getFactory();

      const reportCreationTransaction = factory.newTransaction(namespace, 'ReportCreationTransaction');
      reportCreationTransaction.creator = factory.newRelationship(namespace, 'Employee', accountNumber);
      reportCreationTransaction.description = description;
      reportCreationTransaction.ipfsHash = ipfsHash[0].hash;
      reportCreationTransaction.reportId = reportId;

      await businessNetworkConnection.submitTransaction(reportCreationTransaction);

      await businessNetworkConnection.disconnect(cardId);

      return true;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },

  reportUpdatingTransaction: async function (cardId, reportId, file) {

    try {

      const ipfsHash = await ipfs.add(file.buffer);

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      factory = businessNetworkConnection.getBusinessNetwork().getFactory();

      const reportUpdatingTransaction = factory.newTransaction(namespace, 'ReportUpdatingTransaction');
      reportUpdatingTransaction.report = factory.newRelationship(namespace, 'Report', reportId);
      reportUpdatingTransaction.newIpfsHash = ipfsHash[0].hash;

      await businessNetworkConnection.submitTransaction(reportUpdatingTransaction);

      await businessNetworkConnection.disconnect(cardId);

      return true;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },

  participantData: async function (cardId) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);
      
      const pingConnection = await businessNetworkConnection.ping();
      const accountNumber = pingConnection.participant.split('#')[1];

      const employeeRegistry = await businessNetworkConnection.getParticipantRegistry(namespace + '.Employee');
      const employee = await employeeRegistry.get(accountNumber);

      await businessNetworkConnection.disconnect(cardId);

      return employee;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },

  getParticipantReports: async function (cardId) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const pingConnection = await businessNetworkConnection.ping();
      const accountNumber = pingConnection.participant.split('#')[1];

      const reportsResults = await businessNetworkConnection.query('selectReportsByEmployee', { employee: 'resource:' + namespace + '.Employee#' + accountNumber });

      await businessNetworkConnection.disconnect(cardId);

      return reportsResults;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error
    }
  },

  getAllEmployees: async function (cardId) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const reportsResults = await businessNetworkConnection.query('selectEmployees');

      await businessNetworkConnection.disconnect(cardId);

      return reportsResults;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error
    }
  },

  getEmployeeReports: async function (cardId, accountNumber) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const reportsResults = await businessNetworkConnection.query('selectReportsByEmployee', { employee: 'resource:' + namespace + '.Employee#' + accountNumber });

      await businessNetworkConnection.disconnect(cardId);

      return reportsResults;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error
    }
  },

  getReportUpdatingTransactionInfo: async function (cardId, reportId) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const reportsResults = await businessNetworkConnection.query('selectReportUpdatingTransaction', { report: 'resource:' + namespace + '.Report#' + reportId });

      await businessNetworkConnection.disconnect(cardId);

      return reportsResults;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error
    }
  },

  getReportCreationTransactionInfo: async function (cardId, reportId) {

    try {

      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      const reportsResults = await businessNetworkConnection.query('selectReportCreationTransaction', { reportId: reportId });

      await businessNetworkConnection.disconnect(cardId);

      return reportsResults;
    }
    catch(err) {
      console.log(err);
      var error = {};
      error.error = err.message;
      return error
    }
  }
}
