import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Container,
    Row,
    Col,
    Table,
    Alert
} from 'reactstrap';

import { getVersionsOfReport } from '../actions';

import * as moment from 'moment';

class Versions extends Component {

    componentDidMount() {

        let { getVersionsOfReport, match } = this.props;
        
        getVersionsOfReport(match.params.id);
    }

    render() {

        let { reports, loading, error, match } = this.props

        return (
            <section className="mb-5">
                <Container className="py-4">
                    <Row className="align-items-center justify-content-center">
                        <h1 className="px-4">{match.params.id}</h1>
                    </Row>
                    <hr className="my-4"/>
                    {loading ? 
                        <Row className="align-items-center justify-content-center">
                            <Col md={10} lg={8}>
                                <Alert color="info">Loading...</Alert> 
                            </Col>
                        </Row>
                        :
                        error ? 
                            <Row className="align-items-center justify-content-center">
                                <Col md={10} lg={8}>
                                    <Alert color="danger">{error}</Alert> 
                                </Col>
                            </Row>
                            :
                            <React.Fragment>
                                <Row className="align-items-center justify-content-center">
                                    <Col md={10} lg={8}>
                                        <h5>Created Report</h5>
                                        {reports.initially.map((report, index) => 
                                            <Table key={index} bordered responsive>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Transaction ID:</th>
                                                    <td>{report.transactionId}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Description:</th>
                                                    <td>{report.description}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">File:</th>
                                                    <td><a target="_blank" rel="noreferrer noopener" href={`http://localhost:8080/ipfs/${report.ipfsHash}`}>{report.ipfsHash}</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Date:</th>
                                                    <td>{moment(report.timestamp).format("DD MMMM YYYY HH:mm:ss")}</td>
                                                </tr>
                                            </tbody>
                                        </Table>)}
                                    </Col>
                                </Row>
                                <Row className="mt-4 align-items-center justify-content-center">
                                    <Col md={10} lg={8}>
                                        <h5>All Versions Of Report{!loading && error === "" && reports.versions.length !== 0 && ' (' + reports.versions.length + ')'}</h5>
                                        {reports.versions.map((report, index) => 
                                            <Table key={index} bordered responsive>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Transaction ID:</th>
                                                    <td>{report.transactionId}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">New File:</th>
                                                    <td><a target="_blank" rel="noreferrer noopener" href={`http://localhost:8080/ipfs/${report.newIpfsHash}`}>{report.newIpfsHash}</a></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Date:</th>
                                                    <td>{moment(report.timestamp).format("DD MMMM YYYY HH:mm:ss")}</td>
                                                </tr>
                                            </tbody>
                                        </Table>)}
                                        {reports.versions.length === 0 && <Alert color="danger">No reports...</Alert> }
                                    </Col>
                                </Row>
                            </React.Fragment>
                    }
                </Container>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        reports: state.reportVersions.reports,
        loading: state.reportVersions.loading,
        error: state.reportVersions.error
    }
}

export default connect(mapStateToProps, { getVersionsOfReport })(Versions);