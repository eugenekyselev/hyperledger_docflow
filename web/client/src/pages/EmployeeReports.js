import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Container,
    Row,
    Col,
    Table,
    Alert
} from 'reactstrap';

import { getEmployeeReports } from '../actions'

class EmployeeReports extends Component {

    componentDidMount() {

        let { getEmployeeReports, match } = this.props;
      
        getEmployeeReports(match.params.id);
    }

    render() {

        let { reports, loading, error } = this.props

        return (
            <section className="mb-5">
                <Container className="py-4">
                    <Row className="align-items-center justify-content-center">
                        <h1 className="px-4 mb-4">All Reports{!loading && error === "" && reports.length !== 0 && ' (' + reports.length + ')'}</h1>
                    </Row>
                    <Row className="align-items-center justify-content-center">
                    {loading ?
                        <Col md={10} lg={8}>
                            <Alert color="info">Loading...</Alert> 
                        </Col>
                        :
                        error ? 
                            <Col md={10} lg={8}>
                                <Alert color="danger">{error}</Alert>
                            </Col>
                            :
                            reports.length === 0 ? 
                                <Col md={10} lg={8}>
                                    <Alert color="danger">No reports...</Alert> 
                                </Col>
                                :
                                <Col md={10} lg={8}>
                                    {reports.map((report, index) => 
                                        <Table key={index} bordered responsive>
                                        <tbody>
                                            <tr>
                                                <th scope="row">Report ID:</th>
                                                <td>{report.id}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Description:</th>
                                                <td>{report.description}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">File:</th>
                                                <td><a target="_blank" rel="noreferrer noopener" href={`http://localhost:8080/ipfs/${report.ipfsHash}`}>{report.ipfsHash}</a></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Versions:</th>
                                                <td><Link to={`/report/${report.id}/versions`}>{report.versions}</Link></td>
                                            </tr>
                                        </tbody>
                                    </Table>)}
                                </Col>
                    }
                    </Row>
                </Container>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        reports: state.employeeReports.reports,
        loading: state.employeeReports.loading,
        error: state.employeeReports.error
    }
}

export default connect(mapStateToProps, { getEmployeeReports })(EmployeeReports);