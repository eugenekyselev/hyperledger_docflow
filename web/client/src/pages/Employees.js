import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Container,
    Row,
    Col,
    Table,
    Alert
} from 'reactstrap';

import { getEmployees } from '../actions'

class Employees extends Component {

    componentDidMount() {

        let { getEmployees } = this.props;
        
        getEmployees();
    }

    render() {

        let { employees, loading, error } = this.props

        return (
            <section className="mb-5">
                <Container className="py-4">
                    <Row className="align-items-center justify-content-center">
                        <h1 className="px-4 mb-4">All Company Employees{!loading && error === "" && employees.length !== 0 && ' (' + employees.length + ')'}</h1>
                    </Row>
                    <Row className="align-items-center justify-content-center">
                        {loading ? 
                            <Col md={10} lg={8}>
                                <Alert color="info">Loading...</Alert> 
                            </Col>
                            :
                            error ? 
                                <Col md={10} lg={8}>
                                    <Alert color="danger">{error}</Alert> 
                                </Col>
                                :
                                employees.length === 0 ? 
                                    <Col md={10} lg={8}>
                                        <Alert color="danger">No employees...</Alert> 
                                    </Col>
                                    :
                                    <Col md={10} lg={8}>
                                        {employees.map((employee, index) => 
                                            <Table key={index} bordered responsive>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Employee ID:</th>
                                                    <td>{employee.id}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Name:</th>
                                                    <td>{employee.firstName + " " + employee.lastName}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Reports:</th>
                                                    <td><Link to={`/reports/${employee.id}`}>{employee.amountOfReports}</Link></td>
                                                </tr>
                                            </tbody>
                                        </Table>)}
                                    </Col>
                        }
                    </Row>
                </Container>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        employees: state.employees.employees,
        loading: state.employees.loading,
        error: state.employees.error
    }
}

export default connect(mapStateToProps, { getEmployees })(Employees);