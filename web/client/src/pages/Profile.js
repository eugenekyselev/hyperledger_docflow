import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Container,
    Row,
    Col,
    Table,
    Alert
} from 'reactstrap';

import CreateReportForm from '../components/forms/CreateReport';
import UpdateReportForm from '../components/forms/UpdateReport';

class Profile extends Component {

    render() {

        let { passport, firstName, lastName, accountNumber, amountOfReports, reports } = this.props

        return (
            <section className="mb-5">
                <Container className="py-4">
                    <Row className="align-items-center justify-content-center">
                        <h1 className="px-4">Welcome, {firstName} {lastName}</h1>
                    </Row>
                    <hr className="my-4"/>
                    <Row>
                        <Col md={6} lg={5}>
                            <h5>My Profile Information</h5>
                            <Table bordered responsive>
                                <tbody>
                                    <tr>
                                        <th scope="row">Participant ID:</th>
                                        <td>{accountNumber}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Passport ID:</th>
                                        <td>{passport}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">First Name:</th>
                                        <td>{firstName}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Last Name:</th>
                                        <td>{lastName}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Col>
                        <Col md={6} lg={7}>
                            <Row>
                                <Col md={6}>
                                    <h5>Upload Report</h5>
                                    <CreateReportForm />
                                </Col>
                                <Col md={6}>
                                    <h5>Update Report</h5>
                                    <UpdateReportForm reportsResults={reports} />
                                </Col>
                            </Row>
                            <h5>My reports{amountOfReports !== 0 && ' (' + amountOfReports + ')'}</h5>
                            {reports.map((report, index) => 
                                <Table key={index} bordered responsive>
                                <tbody>
                                    <tr>
                                        <th scope="row">Report ID:</th>
                                        <td>{report.id}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Description:</th>
                                        <td>{report.description}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">File:</th>
                                        <td><a target="_blank" rel="noreferrer noopener" href={`http://localhost:8080/ipfs/${report.ipfsHash}`}>{report.ipfsHash}</a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Versions:</th>
                                        <td><Link to={`/report/${report.id}/versions`}>{report.versions}</Link></td>
                                    </tr>
                                </tbody>
                            </Table>)}
                            {reports.length === 0 && <Alert color="danger">No reports...</Alert>}
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accountNumber: state.participant.accountNumber,
        passport: state.participant.passport,
        firstName: state.participant.firstName,
        lastName: state.participant.lastName,
        amountOfReports: state.participant.amountOfReports,
        reports: state.participant.reports
    }
}

export default connect(mapStateToProps, { })(Profile);