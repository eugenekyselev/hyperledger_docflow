import React, { Component } from 'react';
import { Container,
    Row,
    Col,
    TabContent, TabPane, Nav, NavItem, NavLink
} from 'reactstrap';

import LoginForm from '../components/forms/Login';
import SignupForm from '../components/forms/Signup';

class Main extends Component {

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          activeTab: '1'
        };
      }
    
      toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }

    render() {

        return (
            <Container className="py-4">
                <Row className="align-items-center justify-content-center">
                    <Col md={6}>
                        <Nav tabs>  
                            <NavItem>
                                <NavLink
                                className={this.state.activeTab === '1' ? "active" : null}
                                onClick={() => { this.toggle('1'); }}
                                >
                                Login
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                className={this.state.activeTab === '2'? "active" : null}
                                onClick={() => { this.toggle('2'); }}
                                >
                                Sign Up
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col sm="12">
                                        <h5 className="pt-4">Enter your passport ID</h5>
                                        <LoginForm />
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="2">
                                <Row>
                                    <Col sm="12">
                                        <h5 className="pt-4">Enter your passport ID, first and last name.</h5>
                                        <SignupForm />
                                    </Col>
                                </Row>
                            </TabPane>
                        </TabContent>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Main;