import { select, call, put, all, take } from "redux-saga/effects";
import { startSubmit, stopSubmit, reset } from 'redux-form'
import { push } from 'connected-react-router'
import axios from "axios";

export function* rootSaga() {
    yield all([
        logoutFlow(),
        participantSignUpFlow(),
        participantLoginFlow(),
        createReportFlow(),
        updateReportFlow(),
        getEmployeesFlow(),
        getVersionsFlow(),
        getEmployeeReportsFlow()
    ])
}

const registerParticipant = (passport, firstName, lastName) => {
    return axios({
      method: "post",
      url: "/api/registerParticipant",
      data: {
        cardId: passport,
        firstName: firstName,
        lastName: lastName
      }
    });
}

const getParticipant = (passport) => {
    return axios({
      method: "post",
      url: "/api/participantData",
      data: {
        cardId: passport
      }
    });
}

const createReport = (formData) => {
    return axios.post("/api/createReport", formData, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    });
}

const updateReport = (formData) => {
    return axios.post("/api/updateReport", formData, {
        headers: {
          'content-type': 'multipart/form-data'
        }
    });
}

const getEmployees = (passport) => {
    return axios({
      method: "post",
      url: "/api/employees",
      data: {
        cardId: passport
      }
    });
}

const getVersions = (passport, reportId) => {
    return axios({
      method: "post",
      url: "/api/versions",
      data: {
        cardId: passport,
        reportId: reportId
      }
    });
}

const getInitially = (passport, reportId) => {
    return axios({
      method: "post",
      url: "/api/initially",
      data: {
        cardId: passport,
        reportId: reportId
      }
    });
}

const getEmployeeReports = (passport, accountNumber) => {
    return axios({
      method: "post",
      url: "/api/employeeReports",
      data: {
        cardId: passport,
        accountNumber: accountNumber
      }
    });
}

function* participantSignUpFlow() {
    while (true) {

        let { passport, firstName, lastName, formId } = yield take("PARTICIPANT_SIGNUP_REQUEST")

        yield put(startSubmit(formId))

        try {

            const registerParticipantResult = yield call(registerParticipant, passport, firstName, lastName);
            if (registerParticipantResult.data.error){
                throw new Error(registerParticipantResult.data.error);
            }

            const getParticipantResult = yield call(getParticipant, passport);
            if (getParticipantResult.data.error){
                throw new Error(getParticipantResult.data.error);
            }

            yield put({
                type: "SET_PARTICIPANT_DATA", 
                accountNumber: getParticipantResult.data.accountNumber, 
                passport: passport, 
                firstName: getParticipantResult.data.firstName, 
                lastName: getParticipantResult.data.lastName, 
                amountOfReports: getParticipantResult.data.amountOfReports, 
                reports: getParticipantResult.data.reports
            })
            
            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* participantLoginFlow() {
    while (true) {

        let { passport, formId } = yield take("PARTICIPANT_LOGIN_REQUEST")

        yield put(startSubmit(formId))

        try {

            const getParticipantResult = yield call(getParticipant, passport);
            if (getParticipantResult.data.error){
                throw new Error(getParticipantResult.data.error);
            }

            yield put({
                type: "SET_PARTICIPANT_DATA", 
                accountNumber: getParticipantResult.data.accountNumber, 
                passport: passport, 
                firstName: getParticipantResult.data.firstName, 
                lastName: getParticipantResult.data.lastName, 
                amountOfReports: getParticipantResult.data.amountOfReports, 
                reports: getParticipantResult.data.reports
            })
            
            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* logoutFlow() {
    while (true) {

        yield take("LOGOUT_REQUEST")

        yield put({type: "RESET_DATA"})
    }
}

function* createReportFlow() {

    while (true) {

        let { file, description, formId } = yield take("CREATE_REPORT_REQUEST")
        const state = yield select();
        const passport = state.participant.passport;

        let formData = new FormData();
        formData.append('file', file[0]);
        formData.append('description', description);
        formData.append('cardId', passport);

        yield put(startSubmit(formId))

        try {

            const createReportResult = yield call(createReport, formData);
            if (createReportResult.data.error){
                throw new Error(createReportResult.data.error);
            }

            const getParticipantResult = yield call(getParticipant, passport);
            if (getParticipantResult.data.error){
                throw new Error(getParticipantResult.data.error);
            }

            yield put({
                type: "SET_PARTICIPANT_DATA", 
                accountNumber: getParticipantResult.data.accountNumber, 
                passport: passport, 
                firstName: getParticipantResult.data.firstName, 
                lastName: getParticipantResult.data.lastName, 
                amountOfReports: getParticipantResult.data.amountOfReports, 
                reports: getParticipantResult.data.reports
            })

            yield put(reset(formId))
            yield put(stopSubmit(formId))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* updateReportFlow() {

    while (true) {

        let { reportId, file, formId } = yield take("UPDATE_REPORT_REQUEST")
        const state = yield select();
        const passport = state.participant.passport;

        let formData = new FormData();
        formData.append('file', file[0]);
        formData.append('cardId', passport);
        formData.append('reportId', reportId);

        yield put(startSubmit(formId))

        try {

            const updateReportResult = yield call(updateReport, formData);
            if (updateReportResult.data.error){
                throw new Error(updateReportResult.data.error);
            }

            const getParticipantResult = yield call(getParticipant, passport);
            if (getParticipantResult.data.error){
                throw new Error(getParticipantResult.data.error);
            }

            yield put({
                type: "SET_PARTICIPANT_DATA", 
                accountNumber: getParticipantResult.data.accountNumber, 
                passport: passport, 
                firstName: getParticipantResult.data.firstName, 
                lastName: getParticipantResult.data.lastName, 
                amountOfReports: getParticipantResult.data.amountOfReports, 
                reports: getParticipantResult.data.reports
            })

            yield put(reset(formId))
            yield put(stopSubmit(formId))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* getEmployeesFlow() {

    while (true) {

        yield take("GET_EMPLOYEES_REQUEST")
        const state = yield select();
        const passport = state.participant.passport;

        yield put({type: "SET_EMPLOYEES_LOADING"})

        try {

            const getEmployeesResult = yield call(getEmployees, passport);
            if (getEmployeesResult.data.error){
                throw new Error(getEmployeesResult.data.error);
            }

            yield put({
                type: "SET_EMPLOYEES", 
                employees: getEmployeesResult.data.employees
            })

        } catch (error) {
            yield put({type: "SET_EMPLOYEES_ERROR", error: error.message}); 
        }
    }
}

function* getVersionsFlow() {

    while (true) {

        let { reportId } = yield take("GET_VERSIONS_REQUEST")
        const state = yield select();
        const passport = state.participant.passport;

        yield put({type: "SET_VERSIONS_LOADING"})

        try {

            const getVersionsResult = yield call(getVersions, passport, reportId);
            if (getVersionsResult.data.error){
                throw new Error(getVersionsResult.data.error);
            }

            const getInitiallyResult = yield call(getInitially, passport, reportId);
            if (getInitiallyResult.data.error){
                throw new Error(getInitiallyResult.data.error);
            }

            yield put({
                type: "SET_VERSIONS", 
                reports: { 
                    initially: getInitiallyResult.data.reports, 
                    versions: getVersionsResult.data.reports
                }
            })

        } catch (error) {
            yield put({type: "SET_VERSIONS_ERROR", error: error.message}); 
        }
    }
}

function* getEmployeeReportsFlow() {

    while (true) {

        let { accountNumber } = yield take("GET_EMPLOYEE_REPORTS_REQUEST")
        const state = yield select();
        const passport = state.participant.passport;

        yield put({type: "SET_EMPLOYEE_REPORTS_LOADING"})

        try {

            const getEmployeeReportsResult = yield call(getEmployeeReports, passport, accountNumber);
            if (getEmployeeReportsResult.data.error){
                throw new Error(getEmployeeReportsResult.data.error);
            }

            yield put({
                type: "SET_EMPLOYEE_REPORTS_DATA",
                reports: getEmployeeReportsResult.data.reports
            })

        } catch (error) {
            yield put({type: "SET_EMPLOYEE_REPORTS_ERROR", error: error.message});  
        }
    }
}