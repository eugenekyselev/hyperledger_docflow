export const signupParticipant = (passport, firstName, lastName, formId) => ({ type: "PARTICIPANT_SIGNUP_REQUEST", passport: passport, firstName: firstName, lastName: lastName, formId: formId });
export const loginParticipant= (passport, formId) => ({ type: "PARTICIPANT_LOGIN_REQUEST", passport: passport, formId: formId });
export const logout = () => ({ type: "LOGOUT_REQUEST" });

export const createReport = (file, description, formId) => ({ type: "CREATE_REPORT_REQUEST", file: file, description: description, formId: formId });
export const updateReport = (reportId, file, formId) => ({ type: "UPDATE_REPORT_REQUEST", reportId: reportId, file: file, formId: formId });

export const getEmployees = () => ({ type: "GET_EMPLOYEES_REQUEST" });
export const getVersionsOfReport = (reportId) => ({ type: "GET_VERSIONS_REQUEST", reportId: reportId });

export const getEmployeeReports = (accountNumber) => ({ type: "GET_EMPLOYEE_REPORTS_REQUEST", accountNumber: accountNumber });
