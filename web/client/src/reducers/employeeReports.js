const initialState = {
    reports: [],
    loading: false,
    error: ""
}

const employeeReportsReducer = (state = initialState, action) => {
    switch (action.type) {
    case "SET_EMPLOYEE_REPORTS_DATA":{
        return {
            reports: action.reports,
            loading: false,
            error: ""
        }
    }
    case "SET_EMPLOYEE_REPORTS_ERROR":{
        return {
            ...state,
            error: action.error,
            loading: false
        }
    }
    case "SET_EMPLOYEE_REPORTS_LOADING":{
        return {
            ...state,
            loading: true,
            error: ""
        }
    }
    case "RESET_DATA":{
        return initialState;
    }
    default:
        return state;
    }
}
    
export default employeeReportsReducer