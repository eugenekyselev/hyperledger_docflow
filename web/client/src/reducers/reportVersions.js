const initialState = {
    reports: {
        versions: [],
        initially: []
    },
    loading: false,
    error: ""
}

const reportVersionsReducer = (state = initialState, action) => {
    switch (action.type) {
    case "SET_VERSIONS":{
        return {
            reports: action.reports,
            loading: false,
            error: ""
        }
    }
    case "SET_VERSIONS_ERROR":{
        return {
            ...state,
            error: action.error,
            loading: false
        }
    }
    case "SET_VERSIONS_LOADING":{
        return {
            ...state,
            loading: true,
            error: ""
        }
    }
    case "RESET_DATA":{
        return initialState;
    }
    default:
        return state;
    }
}
    
export default reportVersionsReducer