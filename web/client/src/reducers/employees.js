const initialState = {
    employees: [],
    loading: false,
    error: ""
}

const employeesReducer = (state = initialState, action) => {
    switch (action.type) {
    case "SET_EMPLOYEES":{
        return {
            employees: action.employees,
            loading: false,
            error: ""
        }
    }
    case "SET_EMPLOYEES_ERROR":{
        return {
            ...state,
            error: action.error,
            loading: false
        }
    }
    case "SET_EMPLOYEES_LOADING":{
        return {
            ...state,
            loading: true,
            error: ""
        }
    }
    case "RESET_DATA":{
        return initialState;
    }
    default:
        return state;
    }
}
    
export default employeesReducer