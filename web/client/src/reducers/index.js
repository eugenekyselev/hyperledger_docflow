import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import participantReducer from './participant'
import employeesReducer from './employees'
import employeeReportsReducer from './employeeReports'
import reportVersionsReducer from './reportVersions'

const reducer = combineReducers({
    form: formReducer,
    participant: participantReducer,
    employees: employeesReducer,
    employeeReports: employeeReportsReducer,
    reportVersions: reportVersionsReducer
})

export default reducer