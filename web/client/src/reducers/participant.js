const initialState = {
    accountNumber: "",
    passport: "",
    firstName: "",
    lastName: "",
    amountOfReports: 0,
    reports: []
}

const participantReducer = (state = initialState, action) => {
    switch (action.type) {
    case "SET_PARTICIPANT_DATA":{
        return {
            accountNumber: action.accountNumber,
            passport: action.passport,
            firstName: action.firstName,
            lastName: action.lastName,
            amountOfReports: action.amountOfReports,
            reports: action.reports
        }
    }
    case "RESET_DATA":{
        return initialState;
    }
    default:
        return state;
    }
}
    
export default participantReducer