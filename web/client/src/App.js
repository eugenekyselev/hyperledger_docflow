import React, { Component } from 'react';
import { Switch } from 'react-router-dom'

import { UserIsNotAuthenticated, UserIsAuthenticated } from './wrappers'

import LayoutRoute from './components/layouts/LayoutRoute';
import AppLayout from './components/layouts/AppLayout';

import Main from './pages/Main';
import Profile from './pages/Profile';
import Employees from './pages/Employees';
import EmployeeReports from './pages/EmployeeReports';
import Versions from './pages/Versions';

class App extends Component {

    render() {
        return (
            <Switch>
                <LayoutRoute exact path="/" layout={AppLayout} component={UserIsNotAuthenticated(Main)} />
                <LayoutRoute exact path="/profile" layout={AppLayout} component={UserIsAuthenticated(Profile)} />
                <LayoutRoute exact path="/employees" layout={AppLayout} component={UserIsAuthenticated(Employees)} />
                <LayoutRoute exact path="/reports/:id" layout={AppLayout} component={UserIsAuthenticated(EmployeeReports)} />
                <LayoutRoute exact path="/report/:id/versions" layout={AppLayout} component={UserIsAuthenticated(Versions)} />
            </Switch>
        );
    }
}

export default App;