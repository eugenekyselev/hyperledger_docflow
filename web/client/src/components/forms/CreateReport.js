import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Alert, Input } from 'reactstrap';

import { createReport } from '../../actions'

class CreateReportForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this);
        this.required = this.required.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (handler) => ({target: {files}}) => handler(files.length ? files : "");

  submit = (values) => {

      let { createReport } = this.props;
      
      createReport(values.file, values.description, "createReportForm");
  }

  renderText = ({ input, label, type, meta: { touched, error } }) => (
    <FormGroup className="mb-4">
        <Input placeholder={label} {...input} type={type}/>
        {touched && error && <div className="text-danger"><small>{error}</small></div>}
    </FormGroup>
  )

  renderFile = ({ handleChange, input: {onChange, onBlur}, type, meta: { touched, error }, ...input }) => (
      <FormGroup className="mb-4">
          <Input onChange={handleChange(onChange)} onBlur={handleChange(onBlur)} type={type} {...input}/>
          {touched && error && <div className="text-danger"><small>{error}</small></div>}
      </FormGroup>
  )

  required = (value) => (value ? undefined : 'It is required field')
 
  render() {

      let { error, handleSubmit, pristine, submitting, invalid } = this.props;
    
      return (
          <div>
              {error && <Alert color="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit(this.submit)}>                 
                <Field name="description" type="textarea" 
                      label="Description"
                      component={this.renderText} 
                      validate={this.required}
                  />
                  <Field name="file" type="file" 
                      component={this.renderFile} 
                      handleChange={this.handleChange}
                      validate={this.required}
                  />
                  <FormGroup>
                    <Button type="submit" disabled={pristine || submitting || invalid} color="primary">Submit</Button> 
                  </FormGroup>
              </Form>
          </div>
      );
  }
};

CreateReportForm = connect(
    null,
    {
        createReport
    }
)(CreateReportForm);

export default reduxForm({
    form: 'createReportForm'
})(CreateReportForm)