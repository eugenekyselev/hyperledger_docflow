import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Alert, Input } from 'reactstrap';

import { loginParticipant } from '../../actions'

class LoginForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this);
        this.required = this.required.bind(this);
    }

  submit = (values) => {

      let { loginParticipant } = this.props;
      
      loginParticipant(values.passport, "loginForm");
  }

  renderField = ({ input, label, className, type, meta: { touched, error } }) => (
      <FormGroup className="mb-4">
          <Input {...input} placeholder={label} className={className} invalid={(touched && error) ? true : false } type={type} autoComplete="off" required/>
          {touched && error && <div className="text-danger"><small>{error}</small></div>}
      </FormGroup>
  )

  required = (value) => (value ? undefined : 'It is required field')
 
  render() {

      let { error, handleSubmit, pristine, submitting, invalid } = this.props;
    
      return (
          <div>
              {error && <Alert color="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit(this.submit)}>                 
                  <Field name="passport" type="text" 
                      component={this.renderField} label="Passport ID" className="py-2 px-4"
                      validate={this.required}
                  />
                  <FormGroup>
                    <Button type="submit" disabled={pristine || submitting || invalid} color="primary">Submit</Button> 
                  </FormGroup>
              </Form>
          </div>
      );
  }
};

LoginForm = connect(
    null,
    {
        loginParticipant
    }
)(LoginForm);

export default reduxForm({
    form: 'loginForm'
})(LoginForm)