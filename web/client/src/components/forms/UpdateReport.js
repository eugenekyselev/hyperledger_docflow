import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Alert, Input } from 'reactstrap';

import { updateReport } from '../../actions'

class UpdateReportForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this);
        this.required = this.required.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (handler) => ({target: {files}}) => handler(files.length ? files : "");

  submit = (values) => {

      let { updateReport } = this.props;
      
      updateReport(values.reportId, values.file, "updateReportForm");
  }

  renderFile = ({ handleChange, input: {onChange, onBlur}, type, meta: { touched, error }, ...input }) => (
    <FormGroup className="mb-4">
        <Input onChange={handleChange(onChange)} onBlur={handleChange(onBlur)} type={type} {...input}/>
        {touched && error && <div className="text-danger"><small>{error}</small></div>}
    </FormGroup>
  )

  renderSelectField = ({ reportsResults, input, label, className, type, meta: { touched, error } }) => (
    <FormGroup className="mb-4">
        <Input {...input} className={className} invalid={(touched && error) ? true : false } type={type} autoComplete="off" required>
            <option value="">{label}</option>
            {reportsResults && reportsResults.map((report, index) => (<option key={index} value={report.id}>{report.id}</option>))}
        </Input>
        {touched && error && <div className="text-danger"><small>{error}</small></div>}
    </FormGroup>
)

  required = (value) => (value ? undefined : 'It is required field')
 
  render() {

      let { error, handleSubmit, pristine, submitting, invalid, reportsResults } = this.props;
    
      return (
          <div>
              {error && <Alert color="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit(this.submit)}>                 
                  <Field name="file" type="file" 
                      component={this.renderFile} 
                      handleChange={this.handleChange}
                      validate={this.required}
                  />
                  <Field name="reportId" type="select" reportsResults={reportsResults}
                      component={this.renderSelectField} label="Choose your report" className="py-2 px-4"
                      validate={this.required}
                  />
                  <FormGroup>
                    <Button type="submit" disabled={pristine || submitting || invalid} color="primary">Submit</Button> 
                  </FormGroup>
              </Form>
          </div>
      );
  }
};

UpdateReportForm = connect(
    null,
    {
        updateReport
    }
)(UpdateReportForm);

export default reduxForm({
    form: 'updateReportForm'
})(UpdateReportForm)