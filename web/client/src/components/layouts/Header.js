import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { VisibleOnlyAuth, HiddenOnlyAuth } from '../../wrappers'

import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu } from 'reactstrap';

import { logout } from '../../actions'

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {

    let { firstName, logout } = this.props;

    const ToAllLink = HiddenOnlyAuth(() => 
      <Container>
          <NavbarBrand><b>Hyperledger</b></NavbarBrand>
      </Container>
    );

    const AuthOnlyLink = VisibleOnlyAuth(() => 
      <Container>
        <NavbarBrand><b>Hyperledger</b></NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem className="mx-4">
              <NavLink tag={Link} to="/profile"><b>Profile</b></NavLink>
            </NavItem>
            <NavItem className="mx-4">
              <NavLink tag={Link} to="/employees"><b>All Employees</b></NavLink>
            </NavItem>
            <UncontrolledDropdown className="mx-4" nav inNavbar>
              <DropdownToggle nav caret>
                <b>{firstName}</b>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem onClick={logout}>
                  Logout
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Container>);

    return (
        <Navbar color="light" light expand="md" className="app-header">
          <ToAllLink />
          <AuthOnlyLink />
        </Navbar>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    firstName: state.participant.firstName
  }
}

export default connect(mapStateToProps, { logout })(Header);