import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper'
import { replace } from 'connected-react-router'

const locationHelper = locationHelperBuilder({});

export const UserIsAuthenticated = connectedRouterRedirect({
    redirectPath: '/',
    allowRedirectBack: false,
    authenticatedSelector: state => state.participant.passport !== "",
    wrapperDisplayName: 'UserIsAuthenticated',
    redirectAction: newLoc => (dispatch) => {
        dispatch(replace(newLoc));
    }
});

export const UserIsNotAuthenticated = connectedRouterRedirect({
    redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/profile',
    allowRedirectBack: false,
    authenticatedSelector: state => state.participant.passport === "",
    wrapperDisplayName: 'UserIsNotAuthenticated',
    redirectAction: newLoc => (dispatch) => {
        dispatch(replace(newLoc));
    }
});

export const VisibleOnlyAuth = connectedAuthWrapper({
    authenticatedSelector: state => state.participant.passport !== "",
    wrapperDisplayName: 'VisibleOnlyAuth'
})

export const HiddenOnlyAuth = connectedAuthWrapper({
    authenticatedSelector: state => state.participant.passport === "",
    wrapperDisplayName: 'HiddenOnlyAuth'
})