# Blockchain Document Management

Simple document management with Hyperledger Fabric blockchain using Hyperledger Composer API and IPFS

## Prerequisite

- Operating Systems: Ubuntu Linux 14.04 / 16.04 LTS (both 64-bit), or Mac OS 10.12
- [Docker](https://www.docker.com/) (version 17.03 or higher)
- [npm](https://www.npmjs.com/)  (v5.x)
- [Node](https://nodejs.org/en/) (version 8.9 or higher - note version 9 is not supported)
- [IPFS](https://ipfs.io/)  (v0.4.17 or higher)
* to install specific Node version you can use [nvm](https://github.com/creationix/nvm)

  Example:
  + 1. `nvm install 8.12.0`
  + 2. `nvm use v8.12.0`
- [Hyperledger Composer](https://hyperledger.github.io/composer/installing/development-tools.html)
  * to install composer cli
    `npm install -g composer-cli@0.20`
  * to install composer-rest-server
    `npm install -g composer-rest-server@0.20`
  * to install generator-hyperledger-composer
    `npm install -g generator-hyperledger-composer@0.20`

## Steps
### 1. Clone the repo

Clone the `Blockchain Document Management` repo locally. In a terminal, run:

`git clone https://gitlab.com/nparfen/blockchain-document-management.git`

### 2. Generate the Business Network Archive

Run the following the command to create the bna file:

```
cd nykredit-network/
npm install
```

### 3. Deploy Network

Start Docker

The fabric setup scripts will be in the `/fabric-dev-servers` directory. Start fabric and create peer admin card:

```
cd fabric-dev-servers/
./downloadFabric.sh
./startFabric.sh
./createPeerAdminCard.sh
```

Now, we are ready to deploy the business network to Hyperledger Fabric. This requires the Hyperledger Composer chaincode to be installed on the peer,then the business network archive (.bna) must be sent to the peer, and a new participant, identity, and associated card must be created to be the network administrator. Finally, the network administrator business network card must be imported for use, and the network can then be pinged to check it is responding.

* First, install the business network:

```
cd ../
composer network install --card PeerAdmin@hlfv1 --archiveFile nykredit-network@0.0.1.bna
```

* Start the business network:

```
composer network start --networkName nykredit-network --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card
```

* Import the network administrator identity as a usable business network card:
```
composer card import --file networkadmin.card
```

* Check that the business network has been deployed successfully, run the following command to ping the network:
```
composer network ping --card admin@nykredit-network
```

If the command returns successfully, your setup is complete.

### 4. Run IPFS

```
ipfs init
ipfs daemon
```

### 5. Run Application

Go into the `web` folder and install the dependency:

```
cd web/
npm install
```

Then go to the `client` folder, install the dependency and go back:
```
cd client/
npm install
cd ../
```

Then start the application:
```
npm run dev
```

The application should now be running at:
`http://localhost:3000`